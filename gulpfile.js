const gulp = require("gulp");
const sass = require("gulp-sass");
const browserSync = require("browser-sync").create();
const concat = require("gulp-concat");
const uglify = require("gulp-uglify-es").default;
const sourcemaps = require("gulp-sourcemaps");
const rename = require("gulp-rename");
const clean = require("gulp-clean");
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const gulpSequence = require('gulp-sequence');
const cssmin = require('gulp-cssmin');

gulp.task("scss", function() {
  return gulp
    .src("./dist/scss/*.scss")
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("./app/css"));
});

gulp.task('cssmin', function () {
    gulp.src('app/css/main.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/css'));
});

gulp.task("concat", function() {
    return gulp.src('./dist/js/*.js')
    .pipe(concat('main.js'))
    .pipe(gulp.dest('./app/js'));
});

gulp.task('clean', function () {
  return gulp.src('./app/', {read: false})
      .pipe(clean());
});

gulp.task("uglify", ['concat'], function () {
    return gulp.src("./app/js/main.js")
        .pipe(uglify())
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest("./app/js"));
});

gulp.task('img', function() {
  return gulp.src('./dist/img/*.*')
          .pipe(imagemin({
                  interlaced: true,
                  progressive: true,
                  svgoPlugins: [{removeViewBox: false}],
                  use: [pngquant()]
                })
          )
          .pipe(gulp.dest('./app/img'));
});

gulp.task("srv", ["scss","uglify",'img','clean'], function() {
  browserSync.init({
    server: "./"
  });
  gulp.watch("dist/scss/*.scss", ["scss"]).on("change", browserSync.reload);
  gulp.watch("dist/img/*.*", ["img"]).on("change", browserSync.reload);
  gulp.watch("dist/js/*.*", ["js"]).on("change", browserSync.reload);
  gulp.watch("index.html").on("change", browserSync.reload);
});

gulp.task('dev', gulpSequence('clean', 'srv','cssmin'));

gulp.task("on", ["dev"]);
